# InSpec test for recipe auditbeat::service

# The InSpec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

return if os.windows?

describe service 'auditbeat' do
  it { should be_enabled }
  it { should be_running }
end
