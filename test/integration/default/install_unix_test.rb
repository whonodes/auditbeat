# InSpec test for recipe auditbeat::install_unix

# The InSpec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

return if os.windows?

describe package 'auditbeat' do
  it { should be_installed }
end
