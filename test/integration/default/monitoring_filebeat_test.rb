# InSpec test for recipe auditbeat::monitoring_filebeat

# The InSpec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

return unless Dir.exist? '/etc/filebeat'
return unless Dir.exist? '/etc/filebeat/inputs.d'

describe file '/etc/filebeat/inputs.d/auditbeat.yml' do
  it { should exist }
end
