# InSpec test for recipe auditbeat::monitoring_telegraf

# The InSpec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

return unless Dir.exist? '/etc/telegraf'

describe file '/etc/telegraf/telegraf.d/input_auditbeat_mon.conf' do
  it { should exist }
  its('group') { should eq 'telegraf' }
  its('owner') { should eq 'telegraf' }
end
