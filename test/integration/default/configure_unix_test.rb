# InSpec test for recipe auditbeat::configure_unix

# The InSpec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

return if os.windows?

#### Directories ####
describe directory '/etc/auditbeat' do
  it { should exist }
  its('group') { should eq 'root' }
  its('owner') { should eq 'root' }
end

describe directory '/etc/auditbeat/audit.rules.d' do
  it { should exist }
  its('group') { should eq 'root' }
  its('owner') { should eq 'root' }
end

describe directory '/var/lib/auditbeat' do
  it { should exist }
  its('group') { should eq 'root' }
  its('owner') { should eq 'root' }
end

describe directory '/var/log/auditbeat' do
  it { should exist }
  its('group') { should eq 'root' }
  its('owner') { should eq 'root' }
end

describe directory '/usr/share/auditbeat' do
  it { should exist }
  its('group') { should eq 'root' }
  its('owner') { should eq 'root' }
end

#### Files ####
describe file '/etc/auditbeat/auditbeat.yml' do
  it { should exist }
  its('group') { should eq 'root' }
  its('owner') { should eq 'root' }
end

describe file '/etc/auditbeat/fields.yml' do
  it { should exist }
  its('group') { should eq 'root' }
  its('owner') { should eq 'root' }
end

describe file '/etc/auditbeat/audit.rules.d/base-rules.conf' do
  it { should exist }
  its('group') { should eq 'root' }
  its('owner') { should eq 'root' }
end

describe file '/etc/auditbeat/audit.rules.d/sample-rules.conf.disabled' do
  it { should_not exist }
end
