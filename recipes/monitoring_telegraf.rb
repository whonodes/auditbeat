# Cookbook:: auditbeat
# Recipe:: monitoring_telegraf
#
# Copyright:: 2019, The Authors, All Rights Reserved.

return unless Dir.exist? '/etc/telegraf'

template '/etc/telegraf/telegraf.d/input_auditbeat_mon.conf' do
  action :create
  owner 'telegraf'
  group 'telegraf'
  source 'monitoring_telegraf.conf.erb'
  notifies :create, 'file[/etc/telegraf/service_restart_required]', :immediately
end

file '/etc/telegraf/service_restart_required' do
  action :nothing
  content '1'
  owner 'telegraf'
  group 'telegraf'
end
