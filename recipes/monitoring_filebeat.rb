# Cookbook:: auditbeat
# Recipe:: monitoring_filebeat
#
# Copyright:: 2019, The Authors, All Rights Reserved.

template '/etc/filebeat/inputs.d/auditbeat.yml' do
  action :create
  owner 'root'
  group 'root'
  source 'monitoring_filebeat.conf.erb'
  not_if platform_family? 'windows'
  only_if { Dir.exist? '/etc' }
  only_if { Dir.exist? '/etc/filebeat' }
  only_if { Dir.exist? '/etc/filebeat/inputs.d' }
end

template 'C:\Program Files\Filebeat\inputs.d\auditbeat.yml' do
  action :create
  source 'monitoring_filebeat.conf.erb'
  only_if platform_family? 'windows'
  only_if { Dir.exist? 'C:\Program Files' }
  only_if { Dir.exist? 'C:\Program Files\Filebeat' }
  only_if { Dir.exist? 'C:\Program Files\Filebeat\inputs.d' }
end
