# Cookbook:: auditbeat
# Recipe:: install_unix
#
# Copyright:: 2019, The Authors, All Rights Reserved.
package 'auditbeat' do
  action :install
  version node['auditbeat']['version'] unless node['auditbeat']['version'].nil?
  notifies :stop, 'service[auditbeat]', :before
  notifies :start, 'service[auditbeat]', :delayed
end
