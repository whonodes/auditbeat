# Cookbook:: auditbeat
# Recipe:: service
#
# Copyright:: 2019, The Authors, All Rights Reserved.
service 'auditbeat' do
  action [node['auditbeat']['enabled'].to_sym, node['auditbeat']['state'].to_sym]
end
