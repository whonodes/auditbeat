# Cookbook:: auditbeat
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.
include_recipe 'elastic-repo::default'

if node['auditbeat']['package'] == 'install'
  include_recipe 'auditbeat::install_unix'
  include_recipe 'auditbeat::configure_unix'
  include_recipe 'auditbeat::service'
  include_recipe 'auditbeat::monitoring_filebeat'
  include_recipe 'auditbeat::monitoring_telegraf'
else
  include_recipe 'auditbeat::uninstall_unix'
end
