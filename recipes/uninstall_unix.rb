# Cookbook:: auditbeat
# Recipe:: uninstall_unix
#
# Copyright:: 2019, The Authors, All Rights Reserved.
package 'auditbeat' do
  action :remove
end
