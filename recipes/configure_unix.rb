# Cookbook:: auditbeat
# Recipe:: configure_unix
#
# Copyright:: 2019, The Authors, All Rights Reserved.
directory node['auditbeat']['unix']['conf_directory'] do
  action :create
  owner 'root'
  group 'root'
end

directory node['auditbeat']['unix']['conf_directory'] + 'audit.rules.d' do
  action :create
  owner 'root'
  group 'root'
end

directory '/var/log/auditbeat' do
  action :create
end

directory '/usr/share/auditbeat' do
  action :create
end

directory '/var/lib/auditbeat' do
  action :create
  owner 'root'
  group 'root'
end

template '/etc/auditbeat/audit.rules.d/base-rules.conf' do
  action :create
  owner 'root'
  group 'root'
  source 'base-rules.conf.erb'
  notifies :restart, 'service[auditbeat]', :delayed
end

file '/etc/auditbeat/audit.rules.d/sample-rules.conf.disabled' do
  action :delete
end

template '/etc/auditbeat/auditbeat.yml' do
  action :create
  owner 'root'
  group 'root'
  source 'auditbeat.yml.erb'
  notifies :restart, 'service[auditbeat]', :delayed
end
