# frozen_string_literal: true

name 'auditbeat'
maintainer 'Matthew Iverson'
maintainer_email 'matthewdiverson@gmail.com'
license 'MIT'
description 'Installs/Configures auditbeat'
long_description 'Installs/Configures auditbeat'
version '0.1.0'
chef_version '>= 14.0'

depends 'elastic-repo'

%w(centos fedora debian redhat ubuntu).each do |os|
  supports os
end

issues_url 'https://github.com/<insert_org_here>/auditbeat/issues'
source_url 'https://github.com/<insert_org_here>/auditbeat'
