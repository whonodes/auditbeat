node.default['auditbeat']['package'] = 'install'
node.default['auditbeat']['enabled'] = 'enable'
node.default['auditbeat']['state'] = 'start'

node.default['auditbeat']['unix']['conf_directory'] = '/etc/auditbeat/'
node.default['auditbeat']['input_directory'] = true
